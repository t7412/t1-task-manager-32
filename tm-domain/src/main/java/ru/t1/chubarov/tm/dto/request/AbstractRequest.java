package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractRequest implements Serializable {
}
