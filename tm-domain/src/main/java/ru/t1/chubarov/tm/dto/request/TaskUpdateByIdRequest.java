package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

}
