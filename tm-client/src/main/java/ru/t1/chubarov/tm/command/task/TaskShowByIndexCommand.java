package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.chubarov.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER TASK INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber()-1;

        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setIndex(index);
        @NotNull final Task task = getTaskEndpoint().getTaskByIndex(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by index.";
    }

}
