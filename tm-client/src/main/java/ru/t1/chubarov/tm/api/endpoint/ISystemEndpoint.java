package ru.t1.chubarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chubarov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chubarov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chubarov.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

}
