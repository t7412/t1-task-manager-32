package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.api.service.IProjectService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
