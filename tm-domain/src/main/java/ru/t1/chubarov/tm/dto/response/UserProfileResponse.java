package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    @Nullable
    private User user;

    public UserProfileResponse(@Nullable final User user) {
        this.user = user;
    }

}
