package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.model.User;

public interface IAuthService {

    User check(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException;
}
