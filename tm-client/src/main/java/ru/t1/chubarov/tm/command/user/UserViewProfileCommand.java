package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.UserProfileRequest;
import ru.t1.chubarov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.chubarov.tm.dto.response.UserProfileResponse;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "view-user-profile";
    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest();
        @NotNull final User user = getAuthEndPoint().profile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
